// +build ignore

package main

import (
    "os"
    "fmt"
)

var onsNameMap = map[string]string {

    // Carousel
    "ons-carousel": "Carousel",
    "ons-carousel-item": "CarouselItem",

    // Conditional
    "ons-if": "If",

    // Control
    "ons-pull-hook": "PullHook",
    "ons-segment": "Segment",
    "ons-speed-dial": "SpeedDial",
    "ons-speed-dial-item": "SpeedDialItem",

    // Dialog
    "ons-action-sheet": "ActionSheet",
    "ons-action-sheet-button": "ActionSheetButton",
    "ons-alert-dialog": "AlertDialog",
    "ons-dialog": "Dialog",
    "ons-modal": "Modal",
    "ons-popover": "Popover",
    "ons-toast": "Toast",

    // Form
    "ons-button": "Button",
    "ons-checkbox": "Checkbox",
    "ons-fab": "Fab",
    "ons-input": "Input",
    "ons-radio": "Radio",
    "ons-range": "Range",
    "ons-search-input": "SearchInput",
    "ons-select": "Select",
    "ons-switch": "Switch",

    // Gesture
    "ons-gesture-detector": "GestureDetector",

    // Grid
    "ons-col": "Col",
    "ons-row": "Row",

    // List
    "ons-lazy-repeat": "LazyRepeat",
    "ons-list-header": "ListHeader",
    "ons-list-item": "ListItem",
    "ons-list-title": "ListTitle",
    "ons-list": "List",

    // Menu
    "ons-splitter-content": "SplitterContent",
    "ons-splitter-side": "SplitterSide",
    "ons-splitter": "Splitter",

    // Navigation
    "ons-back-button": "BackButton",
    "ons-navigator": "Navigator",

    // Page
    "ons-bottom-toolbar": "BottomToolbar",
    "ons-page": "Page",
    "ons-toolbar-button": "ToolbarButton",
    "ons-toolbar": "Toolbar",

    // Tabbar
    "ons-tab": "Tab",
    "ons-tabbar": "Tabbar",

    // Util
    "ons-template": "Template",

    // Visual
    "ons-card": "Card",
    "ons-icon": "Icon",
    "ons-progress-bar": "ProgressBar",
    "ons-progress-circular": "ProgressCircular",
    "ons-ripple": "Ripple",
}

func main() {

    file, err := os.Create("gonsen.go")
    if err != nil {
        panic(err)
    }
    defer file.Close()

    fmt.Fprint(file, "package ons\n\nimport \"github.com/gopherjs/vecty\"\n")

    for elem, name := range onsNameMap {
        fmt.Fprintf(file, `
// https://ja.onsen.io/v2/api/js/%s.html
func %s(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("%s", markup...)
}
`, elem, name, elem)
    }
}

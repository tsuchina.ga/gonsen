package ons

import "github.com/gopherjs/vecty"

// https://ja.onsen.io/v2/api/js/ons-alert-dialog.html
func AlertDialog(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-alert-dialog", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-modal.html
func Modal(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-modal", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-search-input.html
func SearchInput(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-search-input", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-tabbar.html
func Tabbar(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-tabbar", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-switch.html
func Switch(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-switch", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-gesture-detector.html
func GestureDetector(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-gesture-detector", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-splitter-content.html
func SplitterContent(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-splitter-content", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-carousel.html
func Carousel(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-carousel", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-if.html
func If(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-if", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-toast.html
func Toast(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-toast", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-button.html
func Button(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-button", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-fab.html
func Fab(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-fab", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-range.html
func Range(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-range", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-back-button.html
func BackButton(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-back-button", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-navigator.html
func Navigator(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-navigator", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-carousel-item.html
func CarouselItem(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-carousel-item", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-pull-hook.html
func PullHook(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-pull-hook", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-speed-dial.html
func SpeedDial(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-speed-dial", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-speed-dial-item.html
func SpeedDialItem(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-speed-dial-item", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-checkbox.html
func Checkbox(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-checkbox", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-toolbar-button.html
func ToolbarButton(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-toolbar-button", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-action-sheet-button.html
func ActionSheetButton(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-action-sheet-button", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-list-header.html
func ListHeader(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-list-header", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-popover.html
func Popover(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-popover", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-page.html
func Page(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-page", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-icon.html
func Icon(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-icon", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-progress-bar.html
func ProgressBar(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-progress-bar", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-list.html
func List(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-list", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-bottom-toolbar.html
func BottomToolbar(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-bottom-toolbar", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-tab.html
func Tab(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-tab", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-segment.html
func Segment(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-segment", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-dialog.html
func Dialog(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-dialog", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-input.html
func Input(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-input", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-row.html
func Row(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-row", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-lazy-repeat.html
func LazyRepeat(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-lazy-repeat", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-progress-circular.html
func ProgressCircular(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-progress-circular", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-action-sheet.html
func ActionSheet(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-action-sheet", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-col.html
func Col(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-col", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-toolbar.html
func Toolbar(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-toolbar", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-splitter.html
func Splitter(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-splitter", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-template.html
func Template(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-template", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-card.html
func Card(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-card", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-radio.html
func Radio(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-radio", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-select.html
func Select(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-select", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-list-item.html
func ListItem(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-list-item", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-list-title.html
func ListTitle(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-list-title", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-splitter-side.html
func SplitterSide(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-splitter-side", markup...)
}

// https://ja.onsen.io/v2/api/js/ons-ripple.html
func Ripple(markup ...vecty.MarkupOrChild) *vecty.HTML {
    return vecty.Tag("ons-ripple", markup...)
}
